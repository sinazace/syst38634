package ca.sheridancollege.dancye;

/* 
 * Sammantha Pennells
 * Tanvi Pathak
 * Laura Martinez
*/
public class GRated extends Person {
	
	String member1;
	String member2;
	String member3;

	public GRated(String givenRole, String givenName, String member1, String member2, String member3) {
		super(givenRole, givenName);
		this.member1 = member1;
		this.member2 = member2;
		this.member3 = member3;
	}

	public GRated(String givenRole, String givenName) {
		super(givenRole, givenName);

	}
	
	
	

}
